import { defineNuxtConfig } from 'nuxt';

export default defineNuxtConfig({
  typescript: {
    shim: false,
    strict: true,
    typeCheck: 'build',
  },
  buildModules: ['@nuxtjs/vuetify'],
});
