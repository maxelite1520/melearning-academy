import { createApp } from 'vue';
import App from './App.vue';

// @ts-ignore
import vuetify from './plugins/vuetify';

createApp(App).use(vuetify).mount('#app');
